const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const ngrok = process.env.NGROK_ENABLED === 'true' ? require('ngrok') : null;
const app = express();

// var host_handler = require('hosthandler');
// host_handler.update('localhost', 'http://qb.calvoterguide.com');

var corsOptions = {
  origin: "http://localhost:8081"
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));


const db = require("./app/models");
const Role = db.role;

db.sequelize.sync({force: true}).then(() => {
  console.log('Drop and Resync Db');
  initial();
});



// simple route
app.get("/", (req, res) => {
  res.json({ message: "Testing olivia's login application demo." });
});

//routes
require('./app/routes/auth.routes')(app);
require('./app/routes/user.routes')(app);

// set port, listen for requests
const PORT = process.env.PORT ||3300;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
  if (!ngrok) {    
    console.log("success!");
  }
});

/**
 * Optional : If NGROK is enabled
 */
if (ngrok) {
  console.log('NGROK Enabled');
  ngrok
    .connect({ addr: process.env.PORT || 3300 })
    .then((url) => {
   
      console.log("success!");
    })
    .catch(() => {
      process.exit(1);
    });
}

// });

//first create something in db
function initial() {
  Role.create({
    id: 1,
    name: "user"
  });
 
  Role.create({
    id: 2,
    name: "moderator"
  });
 
  Role.create({
    id: 3,
    name: "admin"
  });
}
